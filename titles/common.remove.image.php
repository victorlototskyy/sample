<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
  require_once("../../engine/engine.load.common.php");

  if (isset($_POST["id"]) && isset($_POST["title_id"])) {
      $id = $_POST["id"];
      $title_id = $_POST["title_id"];

      $images = new \common\images();
      echo $is_default = $images->delete($id, $title_id);
  }
}
?>