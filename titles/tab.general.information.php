<?
//    var_dump($this->data);
//    var_dump($this->services);
//    var_dump($this->titleservices);
//var_dump($this->images);
?>

<div class="panel-body form-horizontal">
    <fieldset>
        <legend>Populate</legend>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="ftypeadd"></label>
            <div class="col-sm-4">
                <div class="rdio rdio-primary">
                    <input type="radio" name="ftypeadd"
                           id="ftypeadd1" <?= ($this->data["typeadd"] == "1" ? 'checked' : '') ?>
                           value="1" class="js-typeadd">
                    <label for="ftypeadd1">Trakt</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="rdio rdio-primary">
                    <input type="radio" name="ftypeadd"
                           id="ftypeadd2" <?= ($this->data["typeadd"] == "2" ? 'checked' : '') ?>
                           value="2" class="js-typeadd">
                    <label for="ftypeadd2">Manually</label>
                </div>
            </div>
        </div>

        <legend>Information</legend>
        <div class="form-group form-inline" id="t-info">
            <label class="col-sm-4 control-label" for="ftitle">Title <span class="asterisk">*</span></label>

            <div class="col-sm-8">
                <input class="form-control" required="" type="text" name="ftitle" id="ftitle"
                       value="<?= $this->data["title"] ?>" placeholder="Type Title...">
                <button type="button" class="btn btn-primary js-check-api js-trakt" disabled="disabled">Check Trakt
                </button>
                <button type="reset" class="btn btn-danger js-clear-form js-trakt">Clear</button>
            </div>

        </div>
        <div id="js-search-radio" style="display: none;">


        </div>

        <div class="form-group js-div-year" style="display: <?= $this->data["id"] ? 'block' : 'none' ?>;">
            <label class="col-sm-4 control-label" for="fyear">Year </label>

            <div class="col-sm-8">
                <input class="form-control" name="fyear" id="fyear" value="<?= $this->data["year"] ?>"
                       placeholder="Type Year...">
            </div>
        </div>

        <div class="form-group js-div-genre" style="display: <?= $this->data["id"] ? 'block' : 'none' ?>;">
            <label class="col-sm-4 control-label" for="fgenre">Genre <span class="asterisk">*</span></label>

            <div class="col-sm-8">
                <select class="form-control" required="" name="fgenre" id="fgenre">
                    <option value="">-- please select --</option>
                    <?
                    foreach ($this->genres as $genre) {
                        ?>
                        <option
                            value="<?= $genre["slug"] ?>" <?= selected($genre["id"], $this->data["genre"]) ?>><?= $genre["name"] ?></option>
                        <?
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group js-div-short-description" style="display: <?= $this->data["id"] ? 'block' : 'none' ?>;">
            <label class="col-sm-4 control-label" for="fshortdescription">Short Description</label>

            <div class="col-sm-8">
            <textarea class="form-control" name="fshortdescription" id="fshortdescription"
                      placeholder="Type Short Description..." rows="5"><?= $this->data["shortdescription"] ?></textarea>
            </div>
        </div>
        <div class="form-group js-div-description" style="display: <?= $this->data["id"] ? 'block' : 'none' ?>;">
            <label class="col-sm-4 control-label" for="fdescription">Description <span class="asterisk">*</span></label>

            <div class="col-sm-8">
            <textarea class="form-control" required="" name="fdescription" id="fdescription"
                      placeholder="Type Description..." rows="5"><?= $this->data["description"] ?></textarea>
            </div>
        </div>
        <div class="form-group js-div-type-manual"
             style="display: <?= ($this->data["typeadd"] == 2) ? "block" : "none" ?>;">
            <label class="col-sm-4 control-label" for="ftypemanual">Type <span class="asterisk">*</span></label>

            <div class="col-sm-8">
                <select class="form-control" name="ftypemanual" id="ftypemanual">
                    <option value="">-- please select --</option>
                    <option value="movie" <?= selected($this->data["type"], 'movie') ?>>Movie</option>
                    <option value="show" <?= selected($this->data["type"], 'show') ?>>Show</option>
                </select>
            </div>
        </div>


        <div class="form-group js-div-active" style="display: <?= $this->data["id"] ? 'block' : 'none' ?>;">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="ckbox ckbox-primary">
                    <input type="checkbox" name="factive"
                           id="factive" <?= ($this->data["active"] == true ? 'checked' : '') ?>
                           value="1">
                    <label for="factive">Active</label>
                </div>
            </div>
        </div>
        <div id="js-div-points" style="display: <?= $this->data["id"] ? 'block' : 'none' ?>">
            <legend>Glow Points</legend>
            <div class="form-group has-success">
                <label class="col-sm-4 control-label" for="fpoints">Points <span class="asterisk">*</span></label>
                <div class="col-sm-2">
                    <input class="form-control input" required name="fpoints" id="fpoints"
                           value="<?= $this->data["glowpoints"] ?>" min="1" max="100">
                </div>
            </div>


        </div>
        <input type="hidden" name="ftype" id="ftype" value="<?= $this->data["type"] ?>">
        <input type="hidden" name="ftrack_ids" id="ftrack_ids" value="<?= $this->data["ids_trakt"] ?>">
        <input type="hidden" name="fids" id="fids" value="<?= $this->data["ids"] ?>">
        <div id="js-services" style="display: <?= ($this->data["id"]) ? 'block' : 'none' ?>">
            <legend>Services</legend>


            <div id="js-service-blocks">
                <?
                if ($this->titleservices) {
                    foreach ($this->titleservices as $ts) {
                        ?>
                        <div class="js-service-block">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="fservice">Service <span
                                        class="asterisk">*</span></label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="afservice[<?= $ts["id"] ?>]">
                                        <option value="">-- please select --</option>
                                        <?
                                        foreach ($this->services as $service) {
                                            ?>
                                            <option
                                                value="<?= $service["id"] ?>" <?= selected($service["id"], $ts["service"]) ?>><?= $service["name"] ?></option>
                                            <?
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <div class="rdio rdio-primary">
                                        <input type="radio" name="afservicetype[<?= $ts["id"] ?>]"
                                               id="afservicetype1[<?= $ts["id"] ?>]" <?= ($ts["servicetype"] == 1) ? 'checked' : '' ?>
                                               value="1" class="js-type-service">
                                        <label for="afservicetype1[<?= $ts["id"] ?>]">SVOD</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="rdio rdio-primary">
                                        <input type="radio" name="afservicetype[<?= $ts["id"] ?>]"
                                               id="fservicetype2[<?= $ts["id"] ?>]" <?= ($ts["servicetype"] == 2) ? 'checked' : '' ?>
                                               value="2" class="js-type-service">
                                        <label for="fservicetype2[<?= $ts["id"] ?>]">Linear</label>
                                    </div>
                                </div>
                                <div class="col-sm-1"><h4><a rel="<?= $ts["id"] ?>" href="javascript:;"
                                                             class="glyphicon glyphicon-remove text-danger js-del-rel"></a>
                                    </h4>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="">&nbsp;</label>
                                <div class="col-sm-2">
                                    <input class="form-control js-date" name="afdate_start[<?= $ts["id"] ?>]"
                                           value="<?= $ts["date_start"] ?>" placeholder="Type Date...">
                                </div>
                                <?
                                $_id = "aftime_start" . $ts["id"];
                                ?>
                                <div class="col-sm-2">
                                    <div class="bootstrap-timepicker">
                                        <input class="form-control js-time"
                                               name="aftime_start[<?= $ts["id"] ?>]" id="<?= $_id ?>"
                                               value="<?= $ts["time_start"] ?>"
                                               placeholder="Type Time...">
                                    </div>
                                </div>
                                <div class="js-svod" style="display: <?= $ts["servicetype"] == 2 ? 'none' : 'block' ?>">
                                    <div class="col-sm-2">
                                        <input class="form-control js-date" name="afdate_finish[<?= $ts["id"] ?>]"
                                               value="<?= $ts["date_finish"] ?>"
                                               placeholder="Type Date...">
                                    </div>
                                    <?
                                    $_id = "aftime_finish" . $ts["id"];
                                    ?>

                                    <div class="col-sm-2">
                                        <div class="bootstrap-timepicker">
                                            <input class="form-control js-time"
                                                   name="aftime_finish[<?= $ts["id"] ?>]" id="<?= $_id ?>"
                                                   value="<?= $ts["time_finish"] ?>"
                                                   placeholder="Type Time...">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="furl">URL </label>
                                <div class="col-sm-8"><input class="form-control" name="afurl[<?= $ts["id"] ?>]"
                                                             value="<?= $ts["url"] ?>"
                                                             placeholder="Type URL..."></div>
                            </div>
                            <hr>
                        </div>
                        <?
                    }
                }
                ?>
            </div>


            <div id="js-service-block" class="js-service-block" style="display: none;">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="fservice">Service <span class="asterisk">*</span></label>
                    <div class="col-sm-3">
                        <select class="form-control" name="fservice">
                            <option value="">-- please select --</option>
                            <?
                            foreach ($this->services as $service) {
                                ?>
                                <option value="<?= $service["id"] ?>"><?= $service["name"] ?></option>
                                <?
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <div class="rdio rdio-primary">
                            <input type="radio" name="fservicetype"
                                   id="fservicetype1" checked value="1" class="js-type-service">
                            <label for="fservicetype1">SVOD</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="rdio rdio-primary">
                            <input type="radio" name="fservicetype"
                                   id="fservicetype2" value="2" class="js-type-service">
                            <label for="fservicetype2">Linear</label>
                        </div>
                    </div>
                    <div class="col-sm-1"><h4><a rel="" href="javascript:;"
                                                 class="glyphicon glyphicon-remove text-danger js-del-rel"></a></h4>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="">&nbsp;</label>
                    <div class="col-sm-2">
                        <input class="form-control js-date" name="fdate_start" value="" placeholder="Type Date...">
                    </div>
                    <div class="col-sm-2">
                        <div class="bootstrap-timepicker">
                            <input class="form-control js-time" name="ftime_start" value=""
                                   placeholder="Type Time...">
                        </div>
                    </div>
                    <div class="js-svod">
                        <div class="col-sm-2">
                            <input class="form-control js-date" name="fdate_finish" value=""
                                   placeholder="Type Date...">
                        </div>
                        <div class="col-sm-2">
                            <div class="bootstrap-timepicker">
                                <input class="form-control js-time" name="ftime_finish" value=""
                                       placeholder="Type Time...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="furl">URL </label>
                    <div class="col-sm-8"><input class="form-control" name="furl" value=""
                                                 placeholder="Type URL..."></div>
                </div
            </div>
            <hr>
        </div>
        <div class="form-group text-right">
            <div class="col-sm-12">
                <button type="button" id="js-add-service" class="btn btn-success">Add Service</button>
            </div>
            <br>
        </div>
        <div id="js-images" style="display: <?= ($this->data["id"]) ? 'block' : 'none' ?>">
            <legend>Images</legend>
            <div id="js-images-from-url"></div>
<!--            <hr>-->
            <?
            if ($this->images) {
                foreach ($this->images as $image) {
                    ?>
                    <div class="form-group js-image-block">
                        <div class="col-sm-2">&nbsp;</div>
                        <div class="col-sm-2">
                            <div class="rdio rdio-primary">
                                <input type="radio" name="default_image" value="<?= $image["id"] ?>"
                                       id="radioPrimary<?= $image["id"] ?>" <?= ($image["file_default"]) ? 'checked' : '' ?>/>
                                <label for="radioPrimary<?= $image["id"] ?>">Default</label>
                            </div>


                        </div>
                        <div class="col-sm-5"><img width="300px"
                                                   src="<?= BASE_PATH ?>images/titles/<?= $image["image"] ?>"></div>
                        <div class="col-sm-1"><h4><a rel="<?= $image["id"] ?>" href="javascript:;"
                                                     class="glyphicon glyphicon-remove text-danger js-del-img"></a></h4>
                        </div>
                        <div class="col-sm-2"></div>
                    </div>
                    <?
                }
                ?>
                <?
            }
            ?>
            <div class="panel-body dropzone" id="js-uploads"></div>
        </div>
    </fieldset>
</div>

<style>
    .bootstrap-timepicker-widget table td input {
        width: 70px;
    }

    .bootstrap-timepicker-widget.dropdown-menu {
        width: auto;
    }

    .rdio {
        margin-top: 10px;
    }

    .js-service-block {
        margin-bottom: 10px;
    }

</style>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $fileindex = r();


        $("div#js-uploads").dropzone({
            url: "/uploads.php",
            addRemoveLinks: true,
            dictRemoveFile: "Remove",
            init: function () {
                this.on("success", function (file, response) {
                    var result = $.parseJSON(response);
                    file.serverId = result.filename;
                    $('<input>').attr({
                        type: 'hidden',
                        name: 'images[]',
                        rel: result.filename,
                        value: result.filenameoriginal + "|" + result.filetype + "|" + result.filename
                    }).appendTo('#_hiddens');
                    //
                });
                this.on("removedfile", function (file) {
                    if (!file.serverId) {
                        return;
                    }
                    $.post("/deletefile.php?id=" + file.serverId);
                    $("#_hiddens").find("input[rel=" + file.serverId + "]").remove();
                });
            }

        });


        $(document).keypress(function (e) {
            if (e.which == 13) {
                $(".js-check-api").click();
            }
        });

        $('.js-time').each(function () {
            $id = ($(this).attr("id"));
            if ($id) {
                $("#" + $id).timepicker({defaultTime: false})
            }
        })


        $('.js-date').datepicker({});

        $(".js-check-api").on("click", function () {
//if needed
//            if (confirm("Are you sure what you want to get data from Trakt.tv?")) {
            $(".js-div-active, .js-div-description, .js-div-short-description, .js-div-genre, .js-div-year, .js-div-points").hide();
            importFromAPI();
//            }
        });

        $("#js-add-service").on("click", function () {
            var $el = $("#js-service-block").clone(true);

            $($el).removeAttr("id");

            var names = ["furl", "fservice", "fservicetype", "fdate_start", "fdate_finish", "ftime_start", "ftime_finish"];
            var _key = r();

            for (var i = 0; i < names.length; i++) {
                var name = "a" + names[i] + "[" + _key + "]";
                var ids = "a" + names[i] + _key;
                $($el).find("select[name=" + names[i] + "]").attr("name", name);

                if (names[i] == "ftime_start" || names[i] == "ftime_finish") {
                    $($el).find("input[name=" + names[i] + "]").attr("name", name).attr("id", ids);
                    __id = "#" + ids;
                    $($el).find(__id).timepicker({defaultTime: "12:00 AM"});
                } else {
                    $($el).find("input[name=" + names[i] + "]").attr("name", name);
                }
            }

            var names2 = ["fservicetype1", "fservicetype2"];
            for (var i = 0; i < names2.length; i++) {
                var _id = "a" + names2[i] + _key;

                $($el).find("input[id=" + names2[i] + "]").attr("id", _id);
                $($el).find("label[for=" + names2[i] + "]").attr("for", _id);
            }

            $el.find("select").attr("required", "required");
            $el.find(".js-del-rel").attr("rel", _key);
            $el.find('.js-date').attr("id", '').removeClass('hasDatepicker').datepicker();


            $($el).show().appendTo("#js-service-blocks");
        })


        $("#js-images").on("click", ".js-del-img", function () {
            if (confirm("Are you sure?")) {
                title_id = ($("#_id").val());
                id = this.rel;

                url = iURL + "modules/titles/common.remove.image.php";
                $.post(url, {
                        id: id, title_id: title_id
                    }, function (data) {
                        if (data == 1) {
                            $("#js-images").find("input[name=default_image]:first").attr("checked", "checked");
                        }
                    }
                );
            }
            $(this).closest(".js-image-block").remove();

        })

        $("#js-services").on("click", ".js-del-rel", function () {
            if (confirm("Are you sure?")) {
                var reg = /^\d+$/;
                if (this.rel.match(reg)) {
                    title_id = ($("#_id").val());
                    id = this.rel;

                    url = iURL + "modules/titles/common.remove.item.php";
                    $.post(url, {
                        id: id, title_id: title_id
                    });
                }
                $(this).closest(".js-service-block").remove();

            }
        })

        $("#js-image-block-title").on("click", "#js-select-all-img", function () {
        })

        $(".js-image-block-title").on("click", "#js-del-all-img", function () {
            if (confirm("Are you sure?")) {
                $(".js-image-block-title").remove();
            }
        })


        $("#js-services").on("click", ".js-type-service", function () {
            if (this.value == 1) {
                $(this).closest(".js-service-block").find(".js-svod").show();
            } else {
                $(this).closest(".js-service-block").find(".js-svod").hide();
            }

        })


        $("#js-search-radio").on("click", ".js-populate", function () {
            url = iURL + "modules/titles/common.getinfobyid.php";
            id = $("input:radio[name=fselect-in-result]:checked").val();

            if (id) {
                var arr =
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {id: id},
                        beforeSend: function () {
                        },
                        success: function (data) {
                            var result = $.parseJSON(data);
                            if (result) {
                                populateForm(result);
                            } else {
                                //error
                            }
                        }
                    });
            }
        });

        $(document).on("click", ".js-typeadd", function () {
            $p = this.value;
            $("#fa").validate().resetForm();

            if ($p == '1') {

                $("#t-info").addClass("form-inline");
                $(".js-trakt").show();
                $(".js-div-active, .js-div-description, .js-div-short-description, .js-div-genre, .js-div-year").hide();
                $(".js-div-type-manual").hide();
                $("#ftypemanual").attr("required", false);
                $("#js-images").hide();

            } else {
                $("#t-info").removeClass("form-inline");
                $(".js-trakt").hide();
                $(".js-div-active, .js-div-description, .js-div-short-description, .js-div-genre, .js-div-year").show();
                $("#js-div-points").show();
                $(".js-div-type-manual").show();
                $("#ftypemanual").attr("required", true);
                $("#js-services").show();
                $("#js-images").show();
            }
        })

        $(document).on("input keyup", "#ftitle", function () {
            if ($("#ftitle").val().length) {
                $(".js-check-api").attr("disabled", false);
            } else {
                $(".js-check-api").attr("disabled", true);
            }

        });

        $(document).on("keypress", "#fpoints, #fyear", function (e) {
            var charCode = (e.which) ? e.which : e.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
        });


    })

    function r() {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(2);
        };
        return (S4() + S4() + S4());
    }

    function populateForm(data) {

        $("#js-search-radio").empty().hide();
        $("#ftitle").val(data.title);

        $(".js-div-year").show();
        $("#fyear").val(data.year);

        $(".js-div-genre").show();
        $("#fgenre [value='" + data.genres + "']").attr("selected", "selected");

        $("#ftype").val(data.type);
        $("#ftrack_ids").val(data.ids)
        $("#fids").val(data.idsall)

        $("#js-images-from-url").empty();

        if (data.idsall != "") {
            getImageFromAPI(data.idsall);
            setDefaultImage();
        }

        $("#fids").val(data.idsall)


        $(".js-div-description").show();
        $("#fdescription").val(data.description);

        $(".js-div-short-description").show();
        $(".js-div-active").show();
        $("#js-div-points").show();

        $("#js-services").show();
        $("#js-images").show();
    }

    function setDefaultImage() {
    }

    function getImageFromAPI(ids) {
        url = iURL + "modules/titles/common.getimage.php";

        if (title.length > 0) {
            var arr =
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {ids: ids},
                    beforeSend: function () {
                    },
                    success: function (data) {

                        if (data) {
                            $(data).appendTo('#js-images-from-url').show();
                        }
                    }
                });
        }
    }

    function importFromAPI() {

        url = iURL + "modules/titles/common.search.php";
        title = $("#ftitle").val();

        if (title.length > 0) {
            var arr =
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {title: title},
                    beforeSend: function () {
                    },
                    success: function (data) {
                        if (data) {
                            $("#js-search-radio").html(data).show();
                        } else {
                            $("#js-search-radio").html("<p class='lead'>Sorry. We could not find any results :(</p>").show();
                        }
                    }
                });
        }
    }

</script>