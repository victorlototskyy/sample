<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
  require_once("../../engine/engine.load.common.php");



    $search  = new \common\getinfofromapi();
    $id = (isset($_POST["id"]) && !empty($_POST["id"]))?$_POST["id"]:false;

    if ($id) {
      $data = $search->searchById($id);
    } else {
      $data = false;
    }

  $result = $data;

  echo(json_encode($result));
}
?>