<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    require_once("../../engine/engine.load.common.php");


    $image = new \common\getimagefromapi();
    $ids = (isset($_POST["ids"]) && !empty($_POST["ids"])) ? unserialize($_POST["ids"]) : false;
    $data = array();
    $html = "";


    if ($ids) {


        foreach ($ids as $key => $value) {
            if ($key == "imdb") {
                $data = $image->searchById($value);
                break;
            }
        }

    }
//  var_dump($data);

    if ($data["images"]) {

//        $html .= '<div class="form-group js-image-block-title" id="js-image-block-title">
//            <div class="col-sm-2">&nbsp;</div>
//            <div class="col-sm-2">
//            <a rel="" href="javascript:;" id="js-select-all-img" class="text-danger js-select-all-img">Select all</a>            </div>
//            <div class="col-sm-5"></div>
//            <div class="col-sm-3"><a rel="" href="javascript:;" id="js-del-all-img" class="text-danger js-del-all-img">Delete all images.</a></div>
//        </div>';
//
//        $html .= "<hr>";
//        var_dump($data["images"]);

        foreach ($data["images"] as $key => $image) {
            if ($key <= 5) {
                $html .= '<div class="form-group js-image-block">
                <div class="col-sm-2">&nbsp;</div>
                <div class="col-sm-2">
                    <div class="ckbox ckbox-primary">
                        <input class="js-url-image-add" type="checkbox" name="url_image_add[' . $image["id"] . ']" id="furl_image_add' . $image["id"] . '" checked value="' . $image["url"] . '">
                        <label for="furl_image_add' . $image["id"] . '">Add</label>
                    </div>';
//                    <div class="rdio rdio-primary">
//                        <input type="radio" name="url_default_image"  class="js-url-default-image" value="' . $image["id"] . '"
//                               id="radioPrimary' . $image["id"] . '" />
//                        <label for="radioPrimary' . $image["id"] . '">Default</label>
//                        <input type="hidden" name="url_url[' . $image["id"] . ']" value="' . $image["url"] . '">
//                    </div>

                $html .= '</div>
                <div class="col-sm-5"><img width="300px" src="' . $image["url"] . '"></div>
                <div class="col-sm-1"><h4><a rel="' . $image["id"] . '" href="javascript:;"
                                             class="glyphicon glyphicon-remove text-danger js-del-img"></a></h4>
                </div>
                <div class="col-sm-2"></div>
            </div>


';
            }
        }
        $html .= "<br>";
    }

    echo($html);
}
?>