<?php
namespace modules {

    use db\dbpdo as db;
    use backend\SETTINGS as settings;
    use backend\LISTING as listing;
    use secure\SECURE as secure;

    class titles extends \common\abstractmodules
    {
        protected $db;
        protected $_arr = array();
        protected $sort = "id";

        //for sorting
        protected $allowSorting = array(
            "0" => "t.glowpoints",
            "1" => "t.title",
            "2" => "t.year",
            "3" => "t.genre",
            "4" => "t.type",
        );

        protected $_fields = array(
            array("name" => "title", "code" => "ftitle", "required" => true, "type" => "", "actions" => array("add", "update"), "default" => ""),
            array("name" => "genre", "code" => "fgenre", "required" => true, "type" => "", "actions" => array("add", "update"), "default" => ""),
            array("name" => "year", "code" => "fyear", "required" => true, "type" => "", "actions" => array("add", "update"), "default" => ""),
            array("name" => "glowpoints", "code" => "fpoints", "required" => true, "type" => "", "actions" => array("add", "update"), "default" => "50"),
            array("name" => "type", "code" => "ftype", "required" => false, "type" => "", "actions" => array("add", "update"), "default" => "1"),
            array("name" => "ids_trakt", "code" => "ftrack_ids", "required" => false, "type" => "", "actions" => array("add", "update"), "default" => "1"),
            array("name" => "ids", "code" => "fids", "required" => false, "type" => "", "actions" => array("add", "update"), "default" => ""),
            array("name" => "typeadd", "code" => "ftypeadd", "required" => true, "type" => "", "actions" => array("add", "update"), "default" => "1"),
            array("name" => "description", "code" => "fdescription", "required" => true, "type" => "", "actions" => array("add", "update"), "default" => ""),
            array("name" => "shortdescription", "code" => "fshortdescription", "required" => false, "type" => "", "actions" => array("add", "update"), "default" => ""),
            array("name" => "active", "code" => "factive", "required" => false, "type" => "", "actions" => array("add", "update"), "default" => "1")
        );

        public function __construct()
        {
            $this->db = DB::getInstance();
            $this->settings = SETTINGS::getInstance();
            $this->secure = SECURE::GetInstance();
        }

        public function add($arr)
        {
            if (!empty($arr)) {

                $arr["factive"] = (isset($arr["factive"])) ? $arr["factive"] : 0;

                if (isset($arr["fgenre"])) {
                    $_genre = new \modules\genres();
                    $arr["fgenre"] = $_genre->getIdfromSlug($arr["fgenre"]);
                }
                $isValid = ($this->isValid($arr, $this->_fields));

                if ($isValid === true) {
                    $this->db->reset();

                    $arr["factive"] = (isset($arr["factive"])) ? 1 : 0;

                    if ($arr["ftypeadd"] == '2') {
                        $arr["ftype"] = $arr["ftypemanual"];
                    }


                    $this->addFields($arr, $this->_fields, "add");

                    $this->db->assign("date_add", "NOW()", "func");
                    $_last_id = $this->db->insert($this->tblTitles);

                    if (isset($arr["afservice"]) && is_array($arr["afservice"])) {
                        $titleservices = new \common\titlesservices();
                        $titleservices->addServices($arr, $_last_id);
                    }

                    if (isset($arr["url_image_add"])) {
                        $imagesFromUrl = new \common\getimage();
                        foreach ($arr["url_image_add"] as $url) {
                            $_images[] = $imagesFromUrl->getByURL($url);
                        }

                        if ($_images) {
                            $images = new \common\images();
                            $images->add($_images, $_last_id);
                            $images->setDefault($_last_id);
                        }

                    }

                    if (isset($arr["images"]) && !empty($arr["images"])) {
                        $images = new \common\images();
                        $images->add($arr["images"], $_last_id);
                        $images->setDefault($_last_id);
                    }

                    return ($_last_id);
                } else {
                    return false;
                }
            }
        }

        public function update($arr)
        {
            if (!empty($arr)) {
                $data["_id"] = (isset($arr["_id"])) ? $arr["_id"] : false;

                if (isset($arr["fgenre"])) {
                    $_genre = new \modules\genres();
                    $arr["fgenre"] = $_genre->getIdfromSlug($arr["fgenre"]);
                }

                if ($data["_id"]) {
                    $arr["factive"] = (isset($arr["factive"])) ? 1 : 0;

                    if ($arr["ftypeadd"] == '2') {
                        $arr["ftype"] = $arr["ftypemanual"];
                    }

                    $this->db->reset();
                    $this->db->assign("id", $data["_id"]);

                    $this->addFields($arr, $this->_fields, "update");

                    $this->db->assign("date_update", "NOW()", "func");
                    $this->db->update($this->tblTitles, " where id = :id");

                    if (isset($arr["afservice"]) && is_array($arr["afservice"])) {
                        $titleservices = new \common\titlesservices();
                        $titleservices->addServices($arr, $data["_id"]);
                    }

                    $images = new \common\images();
                    if (isset($arr["images"]) && !empty($arr["images"])) {
                        $images->add($arr["images"], $data["_id"]);
                    }

                    if (isset($arr["default_image"])) {
                        $images->setDefault($data["_id"], $arr["default_image"]);
                    } else {
                        $images->setDefault($data["_id"]);
                    }

                    return $this->getRecordAfterUpdate($data["_id"]);
                } else {
                    return false;
                }

            }
        }

        public function delete($id)
        {
            $this->db->reset();
            $this->db->assign("id", $id);

            $query = "select
                  t.id from " . $this->tblTitles . " t
                  where t.id = :id";

            if ($this->db->q($query)->limit()->is_exists()) {

                $this->db->reset();
                $this->db->assign("active", 0);
                $this->db->assign("is_deleted", 1);
                $this->db->assign("date_remove", "NOW()", "func");
                $this->db->assign("id", $id);
                $this->db->update($this->tblTitles, " where id = :id limit 1");

                return true;
            } else {
                return false;
            }
        }

        public function view($id)
        {

        }

        public function getRecords($params = false)
        {
            $_start = isset($params["start"]) ? $params["start"] : 0;
            $_length = isset($params["length"]) ? $params["length"] : 20;

            $_active = (isset($params["active"])) ? $params["active"] : 1;
            $_is_deleted = (isset($params["is_deleted"])) ? $params["is_deleted"] : 0;

            $_search = ((isset($params["search"])) && ($params["search"])) ? $params["search"] : false;

            if (isset($params["order"]["column"]) && $params["order"]["column"] !== false) {
                $this->setSort($params["order"]["column"]);
                $this->setOrder($params["order"]["direction"]);
            }

            $this->db->reset();
            $this->db->assign("active", $_active);
            $this->db->assign("is_deleted", $_is_deleted);

            $query = "select SQL_CALC_FOUND_ROWS
                  t.id
                from " . $this->tblTitles . " t
                where active = :active and
                is_deleted = :is_deleted";

            if ($_search) {
                $_search = $_search . '%';

                $this->db->assign("search", $_search);
                $query .= " and (t.title LIKE :search
                  or t.year LIKE :search
                  or t.glowpoints LIKE :search
                  or t.description LIKE :search
                  ) ";
            }

            $this->db->q($query)->order($this->sort, $this->order)->limit($_length, $_start)->select();


            if ($this->db->result) {
                foreach ($this->db->result as $key => $value) {
                    $_data[] = $value["id"];
                }

                $result["sort"] = $this->sort;
                $result["order"] = $this->order;
                $result["recordsTotal"] = $this->db->getTotalRecords();
                $result["data"] = $this->getRecordsCollection($_data);

                return $result;
            } else {
                return false;
            }
            return false;
        }

        public function getRecordsCollection($id_array)
        {

            $id_array_str = implode(",", $id_array);

            $this->db->reset();

            $query = "select SQL_CALC_FOUND_ROWS
                  t.title,
                  t.year,
                  t.glowpoints,
                  t.type,
                  g.name as 'genre',
                  t.id,
                  t.active,
                  GROUP_CONCAT(p.name SEPARATOR ', ') as 'services'
                from " . $this->tblTitles . " t
                  left join " . $this->tblGenres . " g on g.id = t.genre
                  left join " . $this->tblTitlesServices . " ts on ts.title_id = t.id and  ts.active = '1'
                  left join " . $this->tblProviders . " p on p.id = ts.service
                where t.id in (" . $id_array_str . ")";

            $this->db->q($query)->order($this->sort, $this->order)->group("t.id")->select();

            if ($this->db->result) {
                $result = $this->db->result;
                return $result;
            } else {
                return $result = array();
            }
        }

        public function getRecordsForSelect($params = array())
        {

            $_active = (isset($params["active"])) ? $params["active"] : 1;

            $this->db->reset();
            $this->db->assign("active", $_active);
            $query = "select SQL_CALC_FOUND_ROWS
                  t.title,
                  t.active,
                  t.id
                from " . $this->tblTitles . " t
                where t.active = :active
                order by t.name";
            $this->db->q($query)->select();

            if ($this->db->result) {
                $result = $this->db->result;
            } else {
                $result = array();
            }
            return $result;
        }

        private function getRecordAfterUpdate($id)
        {
            $_active = isset($params["active"]) ? $params["active"] : 1;

            $this->db->reset();
            $this->db->assign("id", $id);
            $this->db->assign("active", $_active);

            $query = "select
                  t.glowpoints,
                  t.title,
                  t.year as 'year',
                  GROUP_CONCAT(p.name SEPARATOR ', ') as 'services',
                  g.name as 'genre',
                  CONCAT(UCASE(LEFT(t.type, 1)),SUBSTRING(t.type, 2)) as 'type'
                from " . $this->tblTitles . " t
                left join " . $this->tblGenres . " g on g.id = t.genre
                left join " . $this->tblTitlesServices . " ts on ts.title_id = t.id
                  left join " . $this->tblProviders . " p on p.id = ts.service
                where
                  t.id = :id
                    and
                      t.active = :active";

            $this->db->q($query)->limit()->group("t.id")->select();
            if ($this->db->result) {
                $result = array_values($this->db->result[0]);
            } else {
                $result = false;
            }

            return array(
                "id" => $id,
                "code" => $this->getClassName(),
                "data" => $result
            );
        }

        public function getRecord($id)
        {
            $this->db->reset();
            $this->db->assign("id", $id);

            $query = "select
                          t.title,
                          t.id,
                          t.year,
                          t.genre,
                          t.type,
                          t.typeadd,
                          t.ids_trakt,
                          t.ids,
                          t.glowpoints,
                          t.shortdescription,
                          t.description,
                          t.active
                      from " . $this->tblTitles . " t
                            where t.id = :id";
            $this->db->q($query)->limit()->select();
            if ($this->db->result) {
                $result["data"] = $this->db->result[0];

                $services = new \common\titlesservices();
                $result["services"] = $services->getRecords(array("title_id" => $id));


            } else {
                $result["services"] = false;
                $result["data"] = array();

                foreach ($this->_fields as $field) {
                    $result["data"][$field["name"]] = $field["default"];
                }

                $result["data"]["id"] = false;
            }

            return $result;
        }

    }
}
?>