<ul class="nav nav-tabs js-tabs">
    <li class="active"><a href="#general_information" class="js-tab-general_information" data-toggle="tab"><strong>General
                Information</strong>&nbsp;</a></li>
    <li style="display: <?= $this->data["id"] ? 'block' : 'none' ?>"><a href="#history" class="js-tab-history"
                                                                        data-toggle="tab"><strong>History</strong>&nbsp;
        </a></li>
</ul>

<div class="tab-content mb6">
    <div class="tab-pane form-horizontal active" id="general_information">
        <?
        require_once("tab.general.information.php");
        ?>
    </div>
    <div class="tab-pane form-horizontal" id="history">
        <?
        require_once("tab.history.php");
        ?>
    </div>

</div>
<div id="_hiddens" style="display: none"></div>
<style>
    .modal-dialog {
        width: 950px;
    }
</style>

