<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    require_once("../../engine/engine.load.common.php");


    $search = new \common\getinfofromapi();
    $query = (isset($_POST["title"]) && !empty($_POST["title"])) ? $_POST["title"] : false;

    if ($query) {
        $data = $search->search($query);

//      var_dump($data);

        if (!empty($data)) {
            $html = "";
            $html .= '<div class="form-group">';
            $html .= '<label class="col-sm-4 control-label" for="fselect-in-result"></label>';
            $html .= '<div class="col-sm-8">';
            foreach ($data as $key => $value) {
                $html .= '<div class="rdio rdio-primary">';
                $html .= '<input type="radio" name="fselect-in-result" id="ftype' . $key . '" ' . ($key == "0" ? "checked" : "") . ' value="' . $value["ids_trakt"] . '">';
                $html .= '<label for="ftype' . $key . '">' . $value["title"] . ' (' . $value["year"] . ')</label>';
                $html .= '</div>';
            }
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="form-group">';
            $html .= '<label class="col-sm-4 control-label" for="fselect-in-result"></label>';
            $html .= '<div class="col-sm-8"><input type="button" class="btn btn-warning js-populate" value="Get Info"></div>';

        } else {
            $html = false;
        }
    } else {
        $html = false;
    }

    echo $html;
}
?>