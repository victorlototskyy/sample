<form action="admin.php?page=<?= $this->page_code ?>" method="POST">
  <div class="table-responsive">
    <table class="table table-hover" id="table_<?= $this->page_code ?>">
      <thead>
      <tr>
        <th>Glow Points</th>
        <th>Title</th>
        <th>Year</th>
        <th>Service</th>
        <th>Genre</th>
        <th>Type</th>
        <th width="100">Action</th>
      </tr>
      </thead>
      <tbody>
<? if ($this->result["data"]) { ?>
      <? foreach ($this->result["data"] as $key=>$value) { ?>
        <tr class="odd gradeA">
          <td><?= $value["glowpoints"] ?></td>
          <td><?= $value["title"] ?></td>
          <td><?= $value["year"] ?></td>
          <td><?= $value["genre"] ?></td>
          <td><?= $value["type"] ?></td>
          <td class="table-action">
            <a class="update" alt="Update record" href="javascript:;" rel="<?= $value["id"] ?>">
              <i class="fa fa-pencil"></i>
            </a>
            <a class="delete-row delete" href="javascript:;" rel="<?= $value["id"] ?>"  data-custom=" you want to delete this record">
              <i class="fa fa-trash-o"></i>
            </a>
          </td>
        </tr>
      <? } ?>
      <? } ?>


      </tbody>
    </table>
  </div>
  <!-- table-responsive -->
</form>
<script>



  jQuery(document).ready(function () {

    var table = $('#table_<?= $this->page_code ?>').dataTable({
      "sPaginationType": "full_numbers",
      "iDisplayLength": 200,
      "processing": true,
      "serverSide": true,
      "oLanguage": { "sSearch": "" },
      "order": [
        [ 0, "asc" ]
      ],
      "ajax": {
        "url": "/__admin/modules/titles/get.records.php",
        "type": "POST"
      },
      "columns": [
        { "data": "glowpoints" },
        { "data": "title" },
        { "data": "year" },
        { "data": "services" },
        { "data": "genre" },
        { "data": "type" },
//                { "data": "id" },
        { "data": "action", className: "table-action" }
      ],
      columnDefs: [
        { orderable: false, targets: [3, -1] }
      ],
      "drawCallback": function (settings) {
//                bindTable();
        bind();
      },
      "aLengthMenu": [
        [200,400,600,1000],
        [200,400,600,1000]
      ]
    });

//        bindTable();

    $("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });

  });


</script>


