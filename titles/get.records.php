<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    require_once("../../engine/engine.load.common.php");

    $params["length"]=isset($_POST["length"]) ?($_POST["length"] * 1) : 20;
    $params["start"]=isset($_POST["start"]) ?($_POST["start"] * 1) : 0;

    $params["order"]["column"]=(isset($_POST["order"][0]["column"])) ?$_POST["order"][0]["column"] : false;
    $params["order"]["direction"]=(isset($_POST["order"][0]["dir"]) && (!is_empty($_POST["order"][0]["dir"]))) ?$_POST["order"][0]["dir"] : "asc";
    $params["search"]=(isset($_POST["search"]["value"]) && (!empty($_POST["search"]["value"]))) ?$_POST["search"]["value"] : false;

    $page_code="titles";

    $_class="\\modules\\" . $page_code;

    $titles=new $_class();
    $data=$titles->getRecords($params);
    $_data=array();

    if (isset($data["data"]) && !empty($data["data"])) {
        foreach ($data["data"] as $key=>$value) {

            $_action='<a class="update" alt="Update record" href="javascript:;" rel="' . $value["id"] . '"><i class="fa fa-pencil"></i></a>
      <a class="delete-row delete" alt="Remove record" href="javascript:;" rel="' . $value["id"] . '"  data-custom=" you want to delete this record"><i class="fa fa-trash-o"></i></a>';

            $_data[]=array(
                "glowpoints"=>$value["glowpoints"],
                "title"=>$value["title"],
                "year"=>$value["year"],
                "services"=>$value["services"],
                "genre"=>$value["genre"],
                "type"=>ucfirst($value["type"]),
                "id"=>$value["id"],
                "action"=>$_action
            );
        }
    } else {
        $data["recordsTotal"]=0;
    }

    if (is_empty($_data)) {
        $data["recordsTotal"]=0;
        $data["recordsTotal"]=0;
    }

//    var_dump($_data);

    $a=array(
        "draw"=>0,
        "recordsTotal"=>$data["recordsTotal"],
        "recordsFiltered"=>$data["recordsTotal"],
        "data"=>$_data);

    echo(json_encode($a));
}







